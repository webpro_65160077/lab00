const nameAgeMap: { [index: string]: number } = {};
nameAgeMap.Jack = 25; 
nameAgeMap.Jaa = 20; 
nameAgeMap.PamPui = 2; 

// nameAgeMap.Mark = "Fifty"; // Error: Type 'string' is not assignable to type 'number'.

console.log(nameAgeMap);