let firstName: string = "Dylan";
// firstName = 33; // attempts to re-assign the value to a different type
console.log(firstName);
console.log(typeof firstName);