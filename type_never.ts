// let x: never = true; 
// Error: Type 'boolean' is not assignable to type 'never'.

let y: undefined = undefined;
let z: null = null;
